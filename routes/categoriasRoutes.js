import express from "express";
import {
  agregar,
  listar,
  eliminar,
  editar,
  buscar,
} from "../controllers/categoriaController.js";
import validarAutenticacion from "../middleware/validarAutenticacion.js";

const router = express.Router();

router.get("/listar", validarAutenticacion, listar);
router.post("/agregar", validarAutenticacion, agregar);
router.put("/editar/:id", validarAutenticacion, editar);
router.delete("/eliminar/:id", validarAutenticacion, eliminar);
router.get("/buscar/:id", validarAutenticacion, buscar);

export default router;
