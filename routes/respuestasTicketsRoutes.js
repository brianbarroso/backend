import express from "express";
import {
  agregar,
  listar,
  eliminar,
  editar,
  buscar,
} from "../controllers/respuestaTicketController.js";
import validarAutenticacion from "../middleware/validarAutenticacion.js";

const router = express.Router();

router.get("/listar", validarAutenticacion, listar);
router.post("/agregar", validarAutenticacion, agregar);
router.put("/", editar);
router.delete("/", eliminar);
router.get("/buscar/:id", validarAutenticacion, buscar);

export default router;
