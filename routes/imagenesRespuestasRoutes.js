import express from "express";
import {
  agregar,
  listar,
  eliminar,
  editar,
  buscar,
} from "../controllers/imagenesRespuestaController.js";

const router = express.Router();

router.get("/", listar);
router.post("/", agregar);
router.put("/", editar);
router.delete("/", eliminar);
router.get("/:id", buscar);

export default router;
