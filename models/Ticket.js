import mongoose from "mongoose";

const ticketSchema = mongoose.Schema(
  {
    idCategoria: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Categoria",
      require: true,
      trim: true,
    },

    idUsuario: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Usuario",
      require: true,
      trim: true,
    },

    numeroTicket: {
      type: Number,
      require: true,
      trim: true,
    },

    asuntoTicket: {
      type: String,
      require: true,
      trim: true,
    },

    descripcionTicket: {
      type: String,
      require: true,
      trim: true,
    },

    prioridadTicket: {
      type: Number, //1=Alto 2= Medio 3 = Bajo
      require: true,
      trim: true,
    },

    fechaTicket: {
      type: String,
      require: true,
      trim: true,
    },

    horaTicket: {
      type: String,
      require: true,
      trim: true,
    },

    estadoTicket: {
      type: Number, //1 = Pendiente 2 = Solucionado
      require: true,
      trim: true,
    },
  },
  { timestamps: true }
);

const Ticket = mongoose.model("Ticket", ticketSchema);
export default Ticket;
