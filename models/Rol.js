import mongoose from "mongoose";

const rolSchema = mongoose.Schema(
  {
    nombreRol: {
      type: String,
      require: true,
      trim: true, //quita los espacio al comienzo  al final
      unique: true,
    },

    estadoRol: {
      type: Number,
      require: true,
      trim: true, //quita los espacio en blanco al comienzo  al final
    },
  },
  { timestamps: true }
);

const Rol = mongoose.model("Rol", rolSchema);
export default Rol;
