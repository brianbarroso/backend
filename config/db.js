import mongoose, { connect } from "mongoose";

const conectarDB = () => {
  const urlConexion = String(process.env.MONGO_URI);

  connect(urlConexion)
    .then((con) => {
      console.log(`Conexion Estalecida con la Base de Datos: ${urlConexion}`);
    })
    .catch((error) => {
      console.log(`no se podido conectar la base de datos ${urlConexion}`);
    });
};

// Configuracion para la conexion a la base de datos
// const conectarDB = async () => {
//   try {
//     const connection = await mongoose.connect(process.env.MONGO_URI, {
//       useNewUrlParser: true,
//       useUnifiedTopology: true,
//     });

//     // Verificamos la conexion mostrando la ip y el puerto de conexion
//     const url = `${connection.connection.host}:${connection.connection.port}`;
//     console.log(`MongoDB Conectado en: ${url}`);
//   } catch (error) {
//     console.log(`Error: ${error.message}`);

//     process.exit(1);
//   }
// };

export default conectarDB;
