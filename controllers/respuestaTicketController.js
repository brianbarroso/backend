import RespuestaTicket from "../models/RespuestaTicket.js";

const agregar = async (req, res) => {
  try {
    const respuesta = new RespuestaTicket(req.body);
    const respuestaGuardada = await respuesta.save();
    res.json({
      body: respuestaGuardada,
      ok: "SI",
      msg: "Registro creado correctamente.",
    });
  } catch (error) {
    console.log(error);
  }
};

const listar = async (req, res) => {
  //console.log("Respondiendo desde el metodo listar desde respuestaTicket");
  try {
    const listarRespuesta = await RespuestaTicket.find();
    res.json(listarRespuesta);
  } catch (error) {
    console.log(error);
  }
};

const eliminar = async (req, res) => {
  console.log("Respondiendo desde el metodo eliminar desde respuestaTicket");
};

const editar = async (req, res) => {
  console.log("Respondiendo desde el metodo Actualizar desde respuestaTicket");
};

const buscar = async (req, res) => {
  const { id } = req.params;

  try {
    const respTicket = await RespuestaTicket.find({
      idTicket: { $eq: id },
    })
      .sort({ fechaRespuestaTicket: -1, horaRespuestaTicket: -1 })
      .populate("idTicket", { _id: 1, numeroTicket: 1 })
      .populate("idUsuario", {
        nombresUsuario: 1,
        _id: 1,
      });
    if (!respTicket) {
      const error = new Error("Registro no encontrado");
      return res.status(400).json({ msg: error.message, ok: "NO" });
    }
    console.log(respTicket);
    res.json(respTicket);
  } catch (error) {
    console.log(error);
  }
};

export { agregar, listar, eliminar, editar, buscar };
