import Rol from "../models/Rol.js";
import dominios from "../helpers/dominios.js";

const agregar = async (req, res) => {
  const { nombreRol } = req.body;
  const exiteRol = await Rol.findOne({ nombreRol });

  if (exiteRol) {
    const error = new Error("Rol ya esta registrado en la base de datos");
    return res.status(400).json({ msg: error.message, ok: "NO" });
  }

  try {
    const rol = new Rol(req.body);
    const rolAlmacenado = await rol.save();
    res.json({
      body: rolAlmacenado,
      ok: "SI",
      msg: "Registro creado correctamente.",
    });
  } catch (error) {
    console.log(error);
  }
};

const listar = async (req, res) => {
  try {
    const roles = await Rol.find();
    res.json(roles);
  } catch (error) {
    console.log(error);
  }
};

const eliminar = async (req, res) => {
  // Recibimos los parametros de la URL
  const { id } = req.params;

  // Validamos si existe el registro a eliminar
  const rol = await Rol.findById(id);

  if (!rol) {
    const error = new Error("Registro no encontrado");
    return res.status(400).json({ msg: error.message, ok: "NO" });
  }

  // Si existe el registro realizamos el proceso de eliminar
  try {
    await rol.deleteOne();
    res.json({ msg: "Registro eliminado con exito.", ok: "SI" });
  } catch (error) {
    console.log(error);
  }
};

const editar = async (req, res) => {
  // Recibimos los parametros de la URL
  const { id } = req.params;

  // Validamos si existe el registro a Actualizar
  const rol = await Rol.findById(id);

  if (!rol) {
    const error = new Error("Registro no encontrado");
    return res.status(400).json({ msg: error.message, ok: "NO" });
  }

  // Captura los datos enviados en el formulario
  rol.nombreRol = req.body.nombreRol || rol.nombreRol;
  rol.estadoRol = req.body.estadoRol || rol.estadoRol;

  // Si existe el registro realizamos el proceso de Actualizar
  try {
    const rolUpdate = await rol.save();
    res.json({
      body: rolUpdate,
      msg: "Registro actualizado con exito.",
      ok: "SI",
    });
  } catch (error) {
    console.log(error);
  }
};

const buscar = async (req, res) => {
  // Recibimos los parametros de la URL
  const { id } = req.params;

  // Validamos si existe el registro a buscar
  const rol = await Rol.findById(id);

  if (!rol) {
    const error = new Error("Registro no encontrado");
    return res.status(400).json({ msg: error.message, ok: "NO" });
  }

  // Si existe el registro se visualiza en pantalla
  res.json(rol);
};

const comboRoles = async (req, res) => {
  try {
    const roles = await Rol.find({ estadoRol: dominios.ESTADO_ACTIVO_ROL });
    res.json(roles);
  } catch (error) {
    console.log(error);
  }
};

export { agregar, listar, eliminar, editar, buscar, comboRoles };
