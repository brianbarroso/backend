import Usuario from "../models/Usuario.js";
import generarJWT from "../helpers/generarJWT.js";
import dominios from "../helpers/dominios.js";

const agregar = async (req, res) => {
  // Evitar usuarios duplicados por el campo usuarioAcceso
  const { usuarioAcceso } = req.body;
  const existeUsuario = await Usuario.findOne({ usuarioAcceso });

  if (existeUsuario) {
    const error = new Error("Usuario ya esta registrado en la base de datos");
    // enviamos respuesta al servdo
    return res.status(400).json({ msg: error.message, ok: "NO" });
  }

  try {
    const usuario = new Usuario(req.body);
    const usuarioGuardado = await usuario.save();
    res.json({ body: usuarioGuardado, ok: "SI" });
  } catch (error) {
    console.log(error);
  }
};

const listar = async (req, res) => {
  try {
    const usuarios = await Usuario.find().populate("idRol", {
      nombreRol: 1,
      _id: 0,
    });
    res.json(usuarios);
  } catch (error) {
    console.log(error);
  }
};

const eliminar = async (req, res) => {
  // Recibimos los parametros de la URL
  const { id } = req.params;

  // Validamos si existe el registro a eliminar
  const usuario = await Usuario.findById(id);

  if (!usuario) {
    const error = new Error("Registro no encontrado");
    return res.status(400).json({ msg: error.message, ok: "NO" });
  }

  // Si existe el registro realizamos el proceso de eliminar
  try {
    await usuario.deleteOne();
    res.json({ msg: "Registro eliminado con exito.", ok: "SI" });
  } catch (error) {
    console.log(error);
  }
};

const editar = async (req, res) => {
  // Recibimos los parametros de la URL
  const { id } = req.params;

  // Validamos si existe el registro a eliminar
  const usuario = await Usuario.findById(id);

  if (!usuario) {
    const error = new Error("Registro no encontrado");
    return res.status(400).json({ msg: error.message, ok: "NO" });
  }

  //capturar los datos que vienen del formulario
  usuario.idRol = req.body.idRol || usuario.idRol;
  usuario.nombresUsuario = req.body.nombresUsuario || usuario.nombresUsuario;
  usuario.celularUsuario = req.body.celularUsuario || usuario.celularUsuario;
  usuario.correoUsuario = req.body.correoUsuario || usuario.correoUsuario;
  usuario.direccionUsuario =
    req.body.direccionUsuario || usuario.direccionUsuario;
  usuario.usuarioAcceso = req.body.usuarioAcceso || usuario.usuarioAcceso;
  usuario.claveAcceso = req.body.claveAcceso || usuario.claveAcceso;
  usuario.estadoUsuario = req.body.estadoUsuario || usuario.estadoUsuario;

  try {
    const usuarioGuardado = await usuario.save();
    res.json({
      body: usuarioGuardado,
      msg: "Documento actualizado correctamente.",
      ok: "SI",
    });
  } catch (error) {
    console.log(error);
  }
};

const buscar = async (req, res) => {
  // Recibimos los parametros de la URL
  const { id } = req.params;

  // Validamos si existe el registro a buscar
  const usuario = await Usuario.findById(id).populate("idRol", {
    nombreRol: 1,
    _id: 1,
  });

  if (!usuario) {
    const error = new Error("Registro no encontrado");
    return res.status(400).json({ msg: error.message, ok: "NO" });
  }

  // Si existe el registro se visualiza en pantalla
  res.json(usuario);
};

const crearCuenta = async (req, res) => {
  // Evitar usuarios duplicados por el campo usuarioAcceso
  const { usuarioAcceso } = req.body;
  const existeUsuario = await Usuario.findOne({ usuarioAcceso });

  if (existeUsuario) {
    const error = new Error("Usuario ya esta registrado en la base de datos");
    // enviamos respuesta al servdo
    return res.status(400).json({ msg: error.message, ok: "NO" });
  }

  try {
    const usuario = new Usuario(req.body);
    const usuarioGuardado = await usuario.save();
    res.json({
      body: usuarioGuardado,
      ok: "SI",
      msg: "Registro creado correctamente",
    });
  } catch (error) {
    console.log(error);
  }
};

const autenticar = async (req, res) => {
  const { usuarioAcceso, claveAcceso } = req.body;

  // comprobar si el usuario existe
  const usuario = await Usuario.findOne({ usuarioAcceso });
  if (!usuario) {
    const error = new Error("Usuario no existe");
    return res
      .status(400)
      .json({ msg: error.message, ok: "Usuario no existe" });
  }
 
  if (await usuario.comprobarClave(claveAcceso)) {
    // Validar que la contraseña exista
    if ((await usuario.estadoUsuario) === 2) {//Valida el estado del usuario
      const error = new Error("Usuario Inactivo");
      return res
        .status(400)
        .json({ msg: error.message, ok: "Usuario Inactivo" });
    } else {
      res.json({
        _id: usuario._id,
        nombresUsuario: usuario.nombresUsuario,
        usuarioAcceso: usuario.usuarioAcceso,
        tokenJwt: generarJWT(usuario._id),
      });
    }
  } else {
    const error = new Error("Usuario o Contraseña incorrecta");
    return res
      .status(400)
      .json({ msg: error.message, ok: "Contraseña incorrecta" });
  }
};

export { agregar, listar, eliminar, editar, buscar, crearCuenta, autenticar };
