import Ticket from "../models/Ticket.js";

const agregar = async (req, res) => {
  const { numeroTicket } = req.body;
  const exiteTicket = await Ticket.findOne({ numeroTicket });

  if (exiteTicket) {
    const error = new Error("Ticket ya esta registrado en la base de datos");
    return res.status(400).json({ msg: error.message, ok: "NO" });
  }

  try {
    const ticket = new Ticket(req.body);
    const ticketAlmacenado = await ticket.save();
    res.json({ body: ticketAlmacenado, ok: "SI" });
  } catch (error) {
    console.log(error);
  }
};

const listar = async (req, res) => {
  try {
    const ticket = await Ticket.find()
      .populate("idCategoria", {
        nombreCategoria: 1,
        _id: 0,
      })
      .populate("idUsuario", {
        nombresUsuario: 1,
        _id: 0,
      })
      .sort({ numeroTicket: -1 });
    res.json(ticket);
  } catch (error) {
    console.log(error);
  }
};

const eliminar = async (req, res) => {
  console.log("Respondiendo desde el metodo eliminar desde ticket");
};

const editar = async (req, res) => {
  console.log("Respondiendo desde el metodo Actualizar desde ticket");
};

const buscar = async (req, res) => {
  // console.log("Respondiendo desde el metodo buscar desde ticket");
  // Recibimos los parametros por la URL
  const { id } = req.params;
  // console.log(id);
  // Validamos si existe el registro a buscar
  const ticket = await Ticket.findById(id)
    .populate("idCategoria", {
      nombreCategoria: 1,
      _id: 1,
    })
    .populate("idUsuario", {
      nombresUsuario: 1,
      _id: 1,
    });
  if (!ticket) {
    const error = new Error("Registro no encontrado");
    return res.status(400).json({ msg: error.message, ok: "NO" });
  }
  // Si existe el registro se visualiza en pantalla
  // console.log(ticket);
  res.json(ticket);
};

// Funcion para buscar el numero mayor de un tickets
const numberTicket = async (req, res) => {
  try {
    const ticket = await Ticket.find().sort({ numeroTicket: -1 });
    // console.log(ticket);
    res.json(ticket);
  } catch (error) {
    console.log(error);
  }
};

export { agregar, listar, eliminar, editar, buscar, numberTicket };
