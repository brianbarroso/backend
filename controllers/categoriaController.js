import Categoria from "../models/Categoria.js";

const agregar = async (req, res) => {
  const { nombreCategoria } = req.body;
  const exiteCategoria = await Categoria.findOne({ nombreCategoria });

  if (exiteCategoria) {
    const error = new Error("Categoria ya esta registrado en la base de datos");
    return res.status(400).json({ msg: error.message, ok: "NO" });
  }

  try {
    const categoria = new Categoria(req.body);
    const categoriaAlmacenado = await categoria.save();
    res.json({ body: categoriaAlmacenado, ok: "SI" });
  } catch (error) {
    console.log(error);
  }
};

const listar = async (req, res) => {
  try {
    const categorias = await Categoria.find();
    res.json(categorias);
  } catch (error) {
    console.log(error);
  }
};

const eliminar = async (req, res) => {
  // Recibimos los parametros de la URL
  const { id } = req.params;

  // Validamos si existe el registro a eliminar
  const categoria = await Categoria.findById(id);

  if (!categoria) {
    const error = new Error("Registro no encontrado");
    return res.status(400).json({ msg: error.message, ok: "NO" });
  }

  // Si existe el registro realizamos el proceso de eliminar
  try {
    await categoria.deleteOne();
    res.json({ msg: "Registro eliminado con exito.", ok: "SI" });
  } catch (error) {
    console.log(error);
  }
};

const editar = async (req, res) => {
  // Recibimos los parametros de la URL
  const { id } = req.params;

  // Validamos si existe el registro a Actualizar
  const categoria = await Categoria.findById(id);

  if (!categoria) {
    const error = new Error("Registro no encontrado");
    return res.status(400).json({ msg: error.message, ok: "NO" });
  }

  // Captura los datos enviados en el formulario
  categoria.nombreCategoria =
    req.body.nombreCategoria || categoria.nombreCategoria;

  // Si existe el registro realizamos el proceso de Actualizar
  try {
    const categoriaUpdate = await categoria.save();
    res.json({
      body: categoriaUpdate,
      msg: "Registro actualizado con exito.",
      ok: "SI",
    });
  } catch (error) {
    console.log(error);
  }
};

const buscar = async (req, res) => {
  // Recibimos los parametros de la URL
  const { id } = req.params;

  // Validamos si existe el registro a buscar
  const categoria = await Categoria.findById(id);

  if (!categoria) {
    const error = new Error("Registro no encontrado");
    return res.status(400).json({ msg: error.message, ok: "NO" });
  }

  // Si existe el registro se visualiza en pantalla
  res.json(categoria);
};

export { agregar, listar, eliminar, editar, buscar };
